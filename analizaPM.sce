/*

Matlab applications in physics
Author: Rafał Osadnik
Technical physics

*/

//Please make sure all data is located in the folder titled "Data" which has to be located where this script's file is

function str = ReadingEnhancer(str) //A function that adds a new line characters in some places for convenient reading
    
    n= 1;
    
    for k = 1:length(str)
        
        if part(str,[k]) == ' ' && k > 70*n then
            
            tempStr1 = part(str,[k:length(str)])
            str = part(str,[1:k]) + "\n"
            str = str + tempStr1
            n = n + 1;
            
        end
        
    end
    
endfunction



Files = list(); //A list that is responsible for containing read files.

for k = 1 : size(listfiles("Data\"))(1)
    
    Files(k) = csvRead("Data\" + listfiles("Data\")(k),";",",","string",[":","."]);//Reading content of the files as strings and substituting some characters for later convenience.
    
end

MaxPM10 = 0; //Setting up helpful variables to determine the max PM10 concentration in given data.
MaxPM10IndexPair = [0,0];

m = 1; //Variables for later indexing 
n = 1;

for k = 1:size(Files) //Going through each file in order to determine the max PM10 concentration, and extract the concentration values for day and night cycles
    
    for l = 2:25
        
        if MaxPM10 < strtod(Files(k)(l,6)) then
            
            MaxPM10 = strtod(Files(k)(l,6)); //Searching for the max PM10 concentration in given data, and saving it's value and information abut when it has occured
            MaxPM10IndexPair = [k,l];
            
        end
        
        if strtod(Files(k)(l,1)) >= 8 && strtod(Files(k)(l,1)) <= 18 then //Dividing the PM10 concentration values into two vectors - for night a day cycles.
            
            Day(m) = strtod(Files(k)(l,6));
            m = m + 1;
        else
            Night(n) = strtod(Files(k)(l,6));
            n = n + 1;
        end
    end
end


NightPM10Avg = mean(Night); //Calculating the mean values of PM10 concentration for each cycle
DayPM10Avg = mean(Day);

NightPM10Sigma = stdev(Night)/sqrt(size(Night)(1)); //Calculating standard deviations of data sets
DayPM10Sigma = stdev(Day)/sqrt(size(Day)(1));

T = abs(NightPM10Avg - DayPM10Avg)/sqrt(NightPM10Sigma^2 + DayPM10Sigma^2); //Conducting Welch's test


resultFile = mopen('PMresults.dat','wt'); //Printing results to a file, and displaying information in command window

mputl("Highest PM10 concentration occured on " + part(listfiles("data")(1),[16:25]) +...
", "+ Files(MaxPM10IndexPair(1))(MaxPM10IndexPair(2),1) +...
 " oclock and reached " + Files(MaxPM10IndexPair(1))(MaxPM10IndexPair(2),6) + "[µg/m3]",resultFile);
 

if T > 3 then
    
    printf(ReadingEnhancer("There is a statistical difference in concentration of PM10 for day and night cycles, the test gave T = %f"),T);
    mputl("There is a statistical difference in concentration of PM10 for day and night cycles, the test gave T = " + string(T),resultFile);
    
elseif T >= 2 && T < 3 then
    
    printf(ReadingEnhancer("Unable to determine if there is a significant statistical difference in concentration of PM10 for day and night cycles, the test gave T = %f"),T);
    mputl("Unable to determine if there is a significant statistical difference in concentration of PM10 for day and night cycles, the test gave T = " + string(T),resultFile);
    
else
    
    printf(ReadingEnhancer("There is not a statistical difference in concentration of PM10 for day and night cycles, the test gave T = %f"),T);
    mputl("There is not a statistical difference in concentration of PM10 for day and night cycles, the test gave T = " + string(T),resultFile);
    
end

mclose(resultFile);





    



